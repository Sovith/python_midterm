from lib2to3.pgen2.token import EQUAL
from tkinter import *
import tkinter
from tkinter import messagebox

root = Tk()


def on_btn_clicked():
    username = entry_username.get()
    password = entry_password.get()
    if username!= "sovith" or password!= "123":
       messagebox.showerror("Message", "Wrong username or password")
    else:
       messagebox.showinfo("Message", "Login Sucessfully")

def close():
   root.quit()

root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=3)

username = StringVar()


lb_username = Label(root, text='Username', font=('Arial', 15))
entry_username = Entry(root, textvariable=username, font=('Arial', 15))

lb_username.grid(column=0, row=0, sticky=W, padx=5, pady=5)
entry_username.grid(column=1,row=0, sticky=E, padx=5, pady=5)

password = StringVar()


lb_password = Label(root, text='Password', font=('Arial', 15))
entry_password = Entry(root, textvariable=password, show="*", font=('Arial', 15))

lb_password.grid(column=0, row=1, sticky=W, padx=5, pady=5)
entry_password.grid(column=1,row=1, sticky=E, padx=5, pady=5)

btn = Button(root, text='Verify', font=('Arial', 15), command=on_btn_clicked)

btn.grid(column=1,row=2, sticky=E, padx=50, pady=5)

btn_exit = Button(root, text='Exit', font=('Arial', 15), command=close)

btn_exit.grid(column=1,row=2, sticky=W)



root.title('Login')
root.geometry('310x140')
root.mainloop()