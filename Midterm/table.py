from logging import root
from tkinter import *
from tkinter.ttk import Treeview


root = root
root = Tk()
root. title('Table')


set = Treeview(root)
set.pack(fill=BOTH, expand=1)

set['columns']= ('id','full_Name','gender','job_title','status')
set.column("#0", width=0, stretch=NO)
set.column("id",anchor=CENTER,width=80)
set.column("full_Name",anchor=CENTER, width=80)
set.column("gender",anchor=CENTER, width=80)
set.column("job_title",anchor=CENTER,width=80)
set.column("status",anchor=CENTER, width=80)

set.heading("#0",text="",anchor=CENTER)
set.heading("id",text="ID",anchor=CENTER)
set.heading("full_Name",text="FULL NAME",anchor=CENTER)
set.heading("gender",text="GENDER",anchor=CENTER)
set.heading("job_title",text="JOB TITLE",anchor=CENTER)
set.heading("status",text="STATUS",anchor=CENTER)

set.insert(parent='',index='end',iid=0,text='',
values=('001','In LeavSovith','Male','Student','Busy'))
set.insert(parent='',index='end',iid=1,text='',
values=('002','Jessica Torida','Female','Model','Busy'))
set.insert(parent='',index='end',iid=2,text='',
values=('003','Peter Parket','Male','SpiderMan','Free'))
set.insert(parent='',index='end',iid=3,text='',
values=('004','Tony Stark','Male','Iron Man','Free'))

root.resizable(FALSE,FALSE)
root.state("zoomed")

root.mainloop()